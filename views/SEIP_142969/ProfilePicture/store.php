<?php
include_once('../../../vendor/autoload.php');
use App\ProfilePicture\ProfilePicture;



if((isset($_FILES['image'])) && (!empty($_FILES['image']['name']))){
    $imageName= time(). $_FILES['image']['name'];
    $temporary_location= $_FILES['image']['tmp_name'];

    move_uploaded_file($temporary_location,'../../Resource/Images/'.$imageName);


    $_POST['image']=$imageName;


}
$objProfilePicture=new ProfilePicture();

$objProfilePicture->setData($_POST);           //set the method


$objProfilePicture->store();                   //store the data into database