<?php
require_once("../../../vendor/autoload.php");
use App\Message\Message;

if(!isset($_SESSION))session_start();
echo Message::getMessage();

?>
<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Profile Picture</title>

    <!-- CSS -->
    <link rel="stylesheet" href="http://fonts.googleapis.com/css?family=Roboto:400,100,300,500">
    <link rel="stylesheet" href="../../../resource/assets/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="../../../resource/assets/font-awesome/css/font-awesome.min.css">
    <link rel="stylesheet" href="../../../resource/assets/css/form-elements.css">
    <link rel="stylesheet" href="../../../resource/assets/css/style.css">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="../../../resource/assets/js/html5shiv.js"></script>
    <script src="../../../resource/assets/js/respond.min.js"></script>
    <![endif]-->

</head>

<body>

<!-- Top content -->
<div class="top-content">
    <div class="container">
        <div class="row">
            <div class="col-sm-6 col-sm-offset-3 form-box">
                <div class="form-top">
                    <div class="form-top-left">
                        <h3>Profile Picture</h3>
                        <p>Enter Name and Profile Picture</p>
                    </div>
                    <div class="form-top-right">
                        <i class="fa fa-picture-o"></i>
                    </div>
                </div>
                <div class="form-bottom">
                    <form role="form" action="store.php" method="post" class="login-form" enctype="multipart/form-data">
                        <div class="form-group">
                            <label class="" for="name">Name :</label>
                            <input type="text" name="name" placeholder="name..." class="form-name form-control" id="form-name">
                        </div>
                        <div class="form-group">
                            <label class=""> Select image to Upload:</label>
                            <input type ="file" name="image" id="image"><br>
                            <button type="submit" class="btn">Create!</button>
                    </form>
                </div>
            </div>
        </div>

    </div>

</div>


<!-- Javascript -->
<script src="../../../resource/assets/js/jquery-1.11.1.min.js"></script>
<script src="../../../resource/assets/bootstrap/js/bootstrap.min.js"></script>
<script src="../../../resource/assets/js/jquery.backstretch.min.js"></script>
<script src="../../../resource/assets/js/scripts.js"></script>

<!--[if lt IE 10]>
<script src="../../../resource/assets/js/placeholder.js"></script>
<![endif]-->
</body>

</html>