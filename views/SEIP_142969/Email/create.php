<?php
    require_once("../../../vendor/autoload.php");
    use App\Message\Message;
    use App\Model;

if(!isset( $_SESSION)) session_start();
echo "<div id =\"message\">". Message::message()."</div>";


?>




<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Email</title>

    <!-- CSS -->
    <link rel="stylesheet" href="http://fonts.googleapis.com/css?family=Roboto:400,100,300,500">
    <link rel="stylesheet" href="../../../Resource/assets/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="../../../Resource/assets/font-awesome/css/font-awesome.min.css">
    <link rel="stylesheet" href="../../../Resource/assets/css/form-elements.css">
    <link rel="stylesheet" href="../../../Resource/assets/css/style.css">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

    <!-- Favicon and touch icons -->
    <link rel="shortcut icon" href="../../../Resource/assets/ico/favicon.png">
    <link rel="apple-touch-icon-precomposed" sizes="144x144" href="../../../Resource/assets/ico/apple-touch-icon-144-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="114x114" href="../../../Resource/assets/ico/apple-touch-icon-114-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="72x72" href="../../../Resource/assets/ico/apple-touch-icon-72-precomposed.png">
    <link rel="apple-touch-icon-precomposed" href="../../../Resource/assets/ico/apple-touch-icon-57-precomposed.png">

</head>

<body>

<!-- Top content -->
<div class="top-content">

    <div class="inner-bg">
        <div class="container">
            <div class="row">
                <div class="col-sm-6 col-sm-offset-3 form-box">
                    <div class="form-top">
                        <div class="form-top-left">
                            <h3>Add your Email</h3>
                            <p>Enter your Name and Email</p>
                        </div>
                        <div class="form-top-right">
                            <i class="fa fa-envelope" aria-hidden="true"></i>
                        </div>
                    </div>
                    <div class="form-bottom">
                        <form role="form" action="store.php" method="post" class="login-form">
                            <div class="form-group">
                                <label class="sr-only" for="name">Name</label>
                                <input type="text" name="name" placeholder="Enter Your Name..." class="form-username form-control" id="form-username">
                            </div>
                            <div class="form-group">
                                <label class="sr-only" for="email">Email</label>
                                <input type="Email" name="email" placeholder="Enter Your Email ..." class="form- form-control" id="form-">
                            </div>
                            <button type="submit" class="btn">Add</button>
                        </form>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-6 col-sm-offset-3 social-login">
                    <div class="social-login-buttons">
                        <a class="btn btn-link-1 btn-link-1-facebook" href="#">
                            <i class="fa fa-edit"></i> Edit
                        </a>
                        <a class="btn btn-link-1 btn-link-1-twitter" href="#">
                            <i class="fa fa-trash"></i> Trash
                        </a>
                        <a class="btn btn-link-1 btn-link-1-google-plus" href="#">
                            <i class="fa fa-download"></i> Download
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>

</div>


<!-- Javascript -->
<script src="../../../Resource/assets/js/jquery-1.11.1.min.js"></script>
<script src="../../../Resource/assets/bootstrap/js/bootstrap.min.js"></script>
<script src="../../../Resource/assets/js/jquery.backstretch.min.js"></script>
<script src="../../../Resource/assets/js/scripts.js"></script>

<!--[if lt IE 10]>
<script src="../../../Resource/assets/js/placeholder.js"></script>
<![endif]-->

</body>

</html>


<script>
    $('#message').show().delay(10).fadeOut();
    $('#message').show().delay(10).fadeIn();
    $('#message').show().delay(10).fadeOut();
    $('#message').show().delay(10).fadeIn();
    $('#message').show().delay(1200).fadeOut();
</script>
