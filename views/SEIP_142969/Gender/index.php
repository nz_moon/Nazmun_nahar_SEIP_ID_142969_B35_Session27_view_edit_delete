<head>
    <link rel="stylesheet" href="../../../Resource/assets/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="../../../Resource/assets/font-awesome/css/font-awesome.min.css">
    <script src="../../../Resource/assets/js/jquery-1.11.1.min.js"></script>
    <script src="../../../Resource/assets/bootstrap/js/bootstrap.min.js"></script>
</head>

<?php
require_once ("../../../vendor/autoload.php");
use App\Gender\Gender;
use App\Message\Message;

$objGender=new Gender();

$allData=$objGender->index("obj");
$serial=1;

echo "<table border='2px'>";
echo "<th style='text-align: center'>Serial</th><th style='text-align: center'>ID</th><th style='text-align: center'>Name</th><th style='text-align: center'>Gender</th><th style='text-align: center'>Action</th>";
foreach($allData as $oneData){
    echo "<tr>";
    echo "<td>$serial</td>";
    echo "<td>$oneData->id</td>";
    echo "<td>$oneData->name</td>";
    echo "<td>$oneData->gender</td>";
    echo "<td>
            <a href='View.php?id=$oneData->id'><button class='btn btn-success'>View</button></a>
            <a href='create.php?id=$oneData->id'><button class='btn btn-success'>Create</button></a>
            <a href='edit.php?id=$oneData->id'><button class='btn btn-info'>Edit</button></a>
            <a href='delete.php?id=$oneData->id'><button class='btn btn-danger'>Delete</button></a>


        </td>";

    echo "</tr>";
    $serial++;
}//End of foreach loop
echo "</table>";
?>


