-- phpMyAdmin SQL Dump
-- version 4.3.11
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Nov 17, 2016 at 12:20 PM
-- Server version: 5.6.24
-- PHP Version: 5.6.8

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `atomic_project_batch35`
--

-- --------------------------------------------------------

--
-- Table structure for table `birthdate`
--

CREATE TABLE IF NOT EXISTS `birthdate` (
  `id` int(11) NOT NULL,
  `name` varchar(200) NOT NULL,
  `date` date NOT NULL,
  `is_deleted` varchar(200) NOT NULL DEFAULT 'No'
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `birthdate`
--

INSERT INTO `birthdate` (`id`, `name`, `date`, `is_deleted`) VALUES
(1, 'Aira Ahmed', '1993-11-02', 'No'),
(2, 'Araf Sikder', '1991-05-09', 'No'),
(3, 'Mahmudul Alam', '1994-09-22', 'No'),
(4, 'Nazmun Nahar', '1995-06-12', 'No'),
(5, 'Nasrin Akter', '1995-06-14', 'No'),
(6, 'Mousumi Akter', '1995-06-13', 'No'),
(7, 'Kamrul Hasan', '1994-05-18', 'No'),
(8, 'dtgfhgkh', '2016-11-15', 'No');

-- --------------------------------------------------------

--
-- Table structure for table `book_title`
--

CREATE TABLE IF NOT EXISTS `book_title` (
  `id` int(11) NOT NULL,
  `sl_no` int(200) NOT NULL,
  `book_title` varchar(200) NOT NULL,
  `author_name` varchar(200) NOT NULL,
  `is_deleted` varchar(200) NOT NULL DEFAULT 'No'
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `book_title`
--

INSERT INTO `book_title` (`id`, `sl_no`, `book_title`, `author_name`, `is_deleted`) VALUES
(1, 1, 'Dairy of sherlock holmes', 'Sir Arthur Conan Doyle', 'No'),
(2, 2, 'Case of sherlock holmes', 'Sir Arthur Conan Doyle', 'No'),
(3, 3, 'Story of sherlock holmes', 'Sir Arthur Conan Doyle', 'No'),
(4, 4, 'Dushcinta mukto notun jibon', 'Dale Carnegie', 'No'),
(5, 5, 'Tobuo Ekdin', 'Sumonto Aslam', 'No'),
(7, 0, 'dfchbj', 'ddgtf', '2016-11-17 15:21:48');

-- --------------------------------------------------------

--
-- Table structure for table `city`
--

CREATE TABLE IF NOT EXISTS `city` (
  `id` int(11) NOT NULL,
  `name` varchar(200) NOT NULL,
  `city` varchar(200) NOT NULL,
  `is_deleted` varchar(200) NOT NULL DEFAULT 'No'
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `city`
--

INSERT INTO `city` (`id`, `name`, `city`, `is_deleted`) VALUES
(1, 'Bangladesh', 'Dhaka', 'No'),
(2, 'Bangladesh', 'Chittagong', 'No'),
(3, 'India', 'Kalkata', 'No');

-- --------------------------------------------------------

--
-- Table structure for table `email`
--

CREATE TABLE IF NOT EXISTS `email` (
  `id` int(11) NOT NULL,
  `name` varchar(200) NOT NULL,
  `email` varchar(200) NOT NULL,
  `is_deleted` varchar(200) NOT NULL DEFAULT 'No'
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `email`
--

INSERT INTO `email` (`id`, `name`, `email`, `is_deleted`) VALUES
(1, 'Nazmun Nahar', 'nazmun.moon84@gmail.com', 'No'),
(2, 'Mahmudul Alam', 'mahmud.alam12@gmail.com', 'No'),
(3, 'Kamrul HAsan', 'kamrul.hasan738@yahoo.com', 'No'),
(4, 'Umme Salma Priya', 'salma.priya57@gmail.com', 'No'),
(5, 'Paromita Dash', 'paromita.dristi84@gmail.com', 'No'),
(6, 'basdshag', 'vgdvegsdh/hdhd/dwduh', 'No'),
(7, 'Moon', 'moon45@yahoo.com', 'No');

-- --------------------------------------------------------

--
-- Table structure for table `gender`
--

CREATE TABLE IF NOT EXISTS `gender` (
  `id` int(11) NOT NULL,
  `name` varchar(200) NOT NULL,
  `gender` varchar(200) NOT NULL,
  `is_deleted` varchar(200) NOT NULL DEFAULT 'No'
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `gender`
--

INSERT INTO `gender` (`id`, `name`, `gender`, `is_deleted`) VALUES
(1, 'Nazmun Nahar', 'Female', 'No'),
(2, 'Paromita Dash', 'Female', 'No'),
(3, 'Umme Salma Priya', 'Female', 'No'),
(4, 'Mahmudul Alam', 'Male', 'No'),
(5, 'Kamrul HAsan', 'Male', 'No');

-- --------------------------------------------------------

--
-- Table structure for table `hobbies`
--

CREATE TABLE IF NOT EXISTS `hobbies` (
  `id` int(11) NOT NULL,
  `name` varchar(200) NOT NULL,
  `hobbies` varchar(200) NOT NULL,
  `is_deleted` varchar(200) NOT NULL DEFAULT 'No'
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `hobbies`
--

INSERT INTO `hobbies` (`id`, `name`, `hobbies`, `is_deleted`) VALUES
(1, 'Priya', 'Playing Badmintton,Drawing,Travelling', 'No'),
(2, 'Moon', 'Playing Badmintton,Chase,Drawing,Travelling', 'No');

-- --------------------------------------------------------

--
-- Table structure for table `profile_picture`
--

CREATE TABLE IF NOT EXISTS `profile_picture` (
  `id` int(11) NOT NULL,
  `name` varchar(200) NOT NULL,
  `picture` varchar(200) NOT NULL,
  `is_deleted` varchar(200) NOT NULL DEFAULT 'No'
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `profile_picture`
--

INSERT INTO `profile_picture` (`id`, `name`, `picture`, `is_deleted`) VALUES
(1, 'Aira Ahmed', 'Desktop\\picture\\1.jpg', 'No'),
(2, 'Alisha Sorker', 'Desktop\\picture\\2.jpg', 'No'),
(3, 'dtgfhgkh', 'gfgfdfbb', 'No'),
(4, 'Moon', 'dfdvdfgf\\hgyjyu\\uj', 'No');

-- --------------------------------------------------------

--
-- Table structure for table `summary_of_organiztion`
--

CREATE TABLE IF NOT EXISTS `summary_of_organiztion` (
  `id` int(11) NOT NULL,
  `organization_name` varchar(200) NOT NULL,
  `organization_summary` varchar(200) NOT NULL,
  `is_deleted` varchar(200) NOT NULL DEFAULT 'No'
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `summary_of_organiztion`
--

INSERT INTO `summary_of_organiztion` (`id`, `organization_name`, `organization_summary`, `is_deleted`) VALUES
(1, 'Bitm', 'To address the skill gap of HR in the industry, BASIS started its own training activities in 2007. Later in 2012, BASIS institutionalized its training activities and set up BASIS Institute of Technolo', 'No'),
(2, 'Besis', 'To address the skill gap of HR in the industry, BASIS started its own training activities in 2007. Later in 2012, BASIS institutionalized its training activities and set up BASIS Institute of Technolo', 'No'),
(3, 'fdgfhhgh', 'sdfghgfd;dehtuerhtejrejfiejriejriw3jri3jrhhrhtuertiyitjurhernfjefnkejerherjfnjenf', 'No'),
(4, 'fdgfhhgh', 'gfvgbknhesdjhghfdiuhfkjsjbvhdygfueiui', 'No'),
(5, 'sadaf', 'gydhmldsdl\r\n', 'No'),
(6, '', '', 'No');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `birthdate`
--
ALTER TABLE `birthdate`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `book_title`
--
ALTER TABLE `book_title`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `city`
--
ALTER TABLE `city`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `email`
--
ALTER TABLE `email`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `gender`
--
ALTER TABLE `gender`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `hobbies`
--
ALTER TABLE `hobbies`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `profile_picture`
--
ALTER TABLE `profile_picture`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `summary_of_organiztion`
--
ALTER TABLE `summary_of_organiztion`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `birthdate`
--
ALTER TABLE `birthdate`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT for table `book_title`
--
ALTER TABLE `book_title`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `city`
--
ALTER TABLE `city`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `email`
--
ALTER TABLE `email`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `gender`
--
ALTER TABLE `gender`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `hobbies`
--
ALTER TABLE `hobbies`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `profile_picture`
--
ALTER TABLE `profile_picture`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `summary_of_organiztion`
--
ALTER TABLE `summary_of_organiztion`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=7;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
