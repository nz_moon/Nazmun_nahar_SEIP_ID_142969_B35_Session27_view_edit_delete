<?php

namespace App\BirthDate;

use App\Message\Message;
use App\Model\Database as DB;
use App\Utility\Utility;
use PDO;

class BirthDate extends  DB{
    public $id;
    public $name;
    public $date;

    public function __construct()
    {
        parent::__construct();
        if(!isset( $_SESSION)) session_start();
    }


        public function setData($postVariableData = NULL)
    {

        if (array_key_exists('id', $postVariableData)) {
            $this->id = $postVariableData['id'];
        }

        if (array_key_exists('name', $postVariableData)) {
            $this->name = $postVariableData['name'];
        }

        if (array_key_exists('date', $postVariableData)) {
            $this->date = $postVariableData['date'];
        }
    }

    public function store()
    {
        $arrData=array($this->name,$this->date);
        $sql = "Insert INTO birthdate(name,date) VALUES(?,?)";
        $STH = $this->DBH->prepare($sql);
        $result=$STH->execute( $arrData);

        if($result)
            Message::setMessage("Success! Data Has Been Insurted Successfully :) ");
        else
            Message::setMessage("Failed! Data Has Not Been Insurted Successfully :( ");

        Utility::redirect('create.php');


    }//end of store mathod

    public function index($fetchMode='ASSOC'){
        $sql = "SELECT * from birthdate where is_deleted = 'No' ";
        $STH = $this->DBH->query($sql);

    
        $fetchMode = strtoupper($fetchMode);
        if(substr_count($fetchMode,'OBJ') > 0)
            $STH->setFetchMode(PDO::FETCH_OBJ);
        else
            $STH->setFetchMode(PDO::FETCH_ASSOC);

        $arrAllData  = $STH->fetchAll();
        return $arrAllData;


    }// end of index();


    public function view($fetchMode='ASSOC'){

        $STH = $this->DBH->query('SELECT * from birthdate WHERE id='.$this->id);

        $fetchMode = strtoupper($fetchMode);
        if(substr_count($fetchMode,'OBJ') > 0)
            $STH->setFetchMode(PDO::FETCH_OBJ);
        else
            $STH->setFetchMode(PDO::FETCH_ASSOC);

        $arroneData  = $STH->fetch();
        return $arroneData;


    }// end of view();

    public function update(){
        $arrData=array($this->name,$this->date);
        $sql="UPDATE birthdate SET name = ?, date = ? WHERE id =".$this->id;

        $STH = $this->DBH->prepare($sql);
       $STH->execute( $arrData);

        Utility::redirect('index.php');



    }//end of update method



    public function delete(){
        $arrData=array($this->name,$this->date);
        $sql="DELETE from birthdate  WHERE id =".$this->id;

        $STH = $this->DBH->prepare($sql);
        $result=$STH->execute( $arrData);

        if($result)
            Message::setMessage("Success! Data Has Been Deleted Successfully :) ");
        else
            Message::setMessage("Failed! Data Has Not Been Deleted Successfully :( ");

        Utility::redirect('index.php');



    }//end of delete method


        public function trash(){

        $sql = "Update birthdate SET is_deleted=NOW() where id=".$this->id;

        $STH = $this->DBH->prepare($sql);

        $STH->execute();

        Utility::redirect('index.php');


    }// end of trash()


}