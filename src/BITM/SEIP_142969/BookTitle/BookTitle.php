<?php


namespace App\BookTitle;

use App\Message\Message;
use App\Model\Database as DB;
use App\Utility\Utility;
use PDO;

class BookTitle extends DB{

    public $id;
    public $book_title;
    public $author_name;

    public function __construct()
    {
        parent::__construct();
        if(!isset( $_SESSION)) session_start();


    }

    public function setData($postVariableData = NULL)
    {

        if (array_key_exists('id', $postVariableData)) {
            $this->id = $postVariableData['id'];
        }

        if (array_key_exists('book_title', $postVariableData)) {
            $this->book_title = $postVariableData['book_title'];
        }

        if (array_key_exists('author_name', $postVariableData)) {
            $this->author_name = $postVariableData['author_name'];
        }
    }

    public function store()
    {
        $arrData=array($this->book_title,$this->author_name);
        $sql = "Insert INTO book_title(book_title,author_name) VALUES(?,?)";
        $STH = $this->DBH->prepare($sql);
        $result=$STH->execute( $arrData);

        if($result)
            Message::setMessage("Success! Data Has Been Insurted Successfully :) ");
        else
            Message::setMessage("Failed! Data Has Not Been Insurted Successfully :( ");

        Utility::redirect('create.php');


    }//end of store mathod


	

    public function index($fetchMode='ASSOC'){

        $sql = "SELECT * from book_title where is_deleted = 'No' ";
        $STH = $this->DBH->query($sql);

        $fetchMode = strtoupper($fetchMode);
        if(substr_count($fetchMode,'OBJ') > 0)
            $STH->setFetchMode(PDO::FETCH_OBJ);
        else
            $STH->setFetchMode(PDO::FETCH_ASSOC);

        $arrAllData  = $STH->fetchAll();
        return $arrAllData;


    }// end of index();


    public function view($fetchMode='ASSOC'){
        $sql = 'SELECT * from book_title where id='.$this->id;

        $STH = $this->DBH->query($sql);

        $fetchMode = strtoupper($fetchMode);
        if(substr_count($fetchMode,'OBJ') > 0)
            $STH->setFetchMode(PDO::FETCH_OBJ);
        else
            $STH->setFetchMode(PDO::FETCH_ASSOC);

        $arroneData  = $STH->fetch();
        return $arroneData;


    }// end of view();


    public function update(){
        $arrData=array($this->book_title,$this->author_name);
        $sql="UPDATE book_title SET book_title = ?, author_name = ? WHERE id =".$this->id;

        $STH = $this->DBH->prepare($sql);
        $result=$STH->execute( $arrData);

        if($result)
            Message::setMessage("Success! Data Has Been Updated Successfully :) ");
        else
            Message::setMessage("Failed! Data Has Not Been Updated Successfully :( ");

        Utility::redirect('index.php');



    }//end of update method



    public function delete(){
        $arrData=array($this->book_title,$this->author_name);
        $sql="DELETE from book_title  WHERE id =".$this->id;

        $STH = $this->DBH->prepare($sql);
        $result=$STH->execute( $arrData);

        if($result)
            Message::setMessage("Success! Data Has Been Deleted Successfully :) ");
        else
            Message::setMessage("Failed! Data Has Not Been Deleted Successfully :( ");

        Utility::redirect('index.php');



    }//end of delete method


        public function trash(){

        $sql = "Update book_title SET is_deleted=NOW() where id=".$this->id;

        $STH = $this->DBH->prepare($sql);

        $STH->execute();

        Utility::redirect('index.php');


    }// end of trash()




}

