<?php

namespace App\Email;

use App\Message\Message;
use App\Model\Database as DB;
use App\Utility\Utility;
use PDO;


class Email extends DB{
    public $id;
    public $name;
    public $email;

    public function __construct()
    {
        parent::__construct();
    }


     public function setData($postVariableData = NULL)
    {

        if (array_key_exists('id', $postVariableData)) {
            $this->id = $postVariableData['id'];
        }

        if (array_key_exists('name', $postVariableData)) {
            $this->name = $postVariableData['name'];
        }

        if (array_key_exists('email', $postVariableData)) {
            $this->email = $postVariableData['email'];
        }
    }

    public function store()
    {
        $arrData=array($this->name,$this->email);
        $sql = "Insert INTO email(name,email) VALUES(?,?)";
        $STH = $this->DBH->prepare($sql);
        $result=$STH->execute( $arrData);

        if($result)
            Message::setMessage("Success! Data Has Been Insurted Successfully :) ");
        else
            Message::setMessage("Failed! Data Has Not Been Insurted Successfully :( ");

        Utility::redirect('create.php');


    }//end of store mathod


    public function index($fetchMode='ASSOC'){

        $STH = $this->DBH->query('SELECT * from email');

        $fetchMode = strtoupper($fetchMode);
        if(substr_count($fetchMode,'OBJ') > 0)
            $STH->setFetchMode(PDO::FETCH_OBJ);
        else
            $STH->setFetchMode(PDO::FETCH_ASSOC);

        $arrAllData  = $STH->fetchAll();
        return $arrAllData;


    }// end of index();


    public function view($fetchMode='ASSOC'){

        $STH = $this->DBH->query('SELECT * from email WHERE id='.$this->id);

        $fetchMode = strtoupper($fetchMode);
        if(substr_count($fetchMode,'OBJ') > 0)
            $STH->setFetchMode(PDO::FETCH_OBJ);
        else
            $STH->setFetchMode(PDO::FETCH_ASSOC);

        $arroneData  = $STH->fetch();
        return $arroneData;


    }// end of view();

     public function update(){

        $arrData = array ($this->name, $this->email);
        $sql = "UPDATE email SET name = ?, email = ? WHERE id =".$this->id;

        $STH = $this->DBH->prepare($sql);
        $STH->execute($arrData);

        Utility::redirect('index.php');

    }// end of update()


    public function delete(){

        $sql = "DELETE from email where id=".$this->id;

        $STH = $this->DBH->prepare($sql);

        $STH->execute();

        Utility::redirect('index.php');

    }// end of delete()


    public function trash(){

        $sql = "UPDATE email SET is_deleted=NOW() where id=".$this->id;

        $STH = $this->DBH->prepare($sql);

        $STH->execute();

        Utility::redirect('index.php');


    }// end of trash()



}

?>